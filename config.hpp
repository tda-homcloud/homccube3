#ifndef HOMCCUBE3_CONFIG_HPP
#define HOMCCUBE3_CONFIG_HPP

namespace homccube {

struct Config {
  int cache_threshold_;
  bool check_apparent_; // must be false now
};

} // namespace homccube

#endif
