#include <gtest/gtest.h>
#include "types.hpp"

using namespace homccube::maxdim3::types;
using homccube::maxdim3::ROOT;
using homccube::maxdim3::INFINITE_LEVEL;

TEST(LinkFind, root) {
  Bitmap bitmap(std::vector<int>{3, 3, 1});
  PD pd;
  LinkFind link_find(bitmap, &pd);

  link_find.parents_[1] = 0;
  link_find.parents_[2] = 1;

  ASSERT_EQ(0, link_find.root(2));
  ASSERT_EQ(0, link_find.root(2));
}

TEST(LinkFind, set_root_1) {
  Bitmap bitmap(std::vector<int>{3, 3, 1});
  PD pd;
  LinkFind link_find(bitmap, &pd);

  link_find.parents_[1] = 0;
  link_find.parents_[2] = 1;

  link_find.set_root(2, 3);
  ASSERT_EQ(3, link_find.parents_[2]);
  ASSERT_EQ(3, link_find.parents_[1]);
  ASSERT_EQ(3, link_find.parents_[0]);
  ASSERT_EQ(ROOT, link_find.parents_[3]);
}

TEST(LinkFind, set_root_2) {
  Bitmap bitmap(std::vector<int>{3, 3, 1});
  PD pd;
  LinkFind link_find(bitmap, &pd);

  link_find.parents_[1] = 0;
  link_find.parents_[2] = 1;

  link_find.set_root(2, 0);
  ASSERT_EQ(0, link_find.parents_[2]);
  ASSERT_EQ(0, link_find.parents_[1]);
  ASSERT_EQ(ROOT, link_find.parents_[0]);
}

TEST(LinkFind, test_1) {
  Bitmap bitmap(std::vector<int>{3, 3, 1});
  bitmap.load({
      0, 1, 2,
      6, 8, 7,
      3, 4, 5
    });
  PD pd;
  LinkFind link_find(bitmap, &pd);

  link_find.compute();
  ASSERT_EQ(2, pd.degrees_.size());
  ASSERT_EQ(0, pd.degrees_[0]);
  ASSERT_EQ(0, pd.degrees_[1]);
  ASSERT_EQ(0, pd.births_[0]);
  ASSERT_EQ(3, pd.births_[1]);
  ASSERT_EQ(INFINITE_LEVEL, pd.deaths_[0]);
  ASSERT_EQ(6, pd.deaths_[1]);

  ASSERT_EQ(4, link_find.survivors_->size());
  EXPECT_EQ(Cube<1>(0, 1, 2, 0, 7), link_find.survivors_->at(0));
  EXPECT_EQ(Cube<1>(0, 0, 1, 0, 8), link_find.survivors_->at(1));
  EXPECT_EQ(Cube<1>(0, 1, 1, 0, 8), link_find.survivors_->at(2));
  EXPECT_EQ(Cube<1>(1, 1, 1, 0, 8), link_find.survivors_->at(3));
}
