#include <gtest/gtest.h>
#include "types.hpp"

using namespace homccube::maxdim3::types;

TEST(Bitmap, load) {
  Bitmap bitmap(std::vector<int>{2, 2, 2});

  bitmap.load({1.1, 0.1,
               3.1, 2.1,

               4.1, 7.1,
               5.1, 6.1,});

  for (int i = 0; i < 8; ++i) {
    std::cout << bitmap.levels_[i] << " ";
  }
  ASSERT_EQ(3.1, bitmap.value(Coord<0>{0, 0, 1, 0}));
  ASSERT_EQ(3, bitmap.level(Coord<0>{0, 0, 1, 0}));
}

TEST(Bitmap, cubes_1) {
  Bitmap bitmap(std::vector<int>{2, 2, 2});
  bitmap.load({1.0, 0.0,
               3.0, 2.0,

               4.0, 7.0,
               5.0, 6.0,});
  std::vector<Cube<1>> cubes = bitmap.cubes<1>();
  std::sort(cubes.begin(), cubes.end());
  ASSERT_EQ(12, cubes.size());

  EXPECT_EQ(Cube<1>({Coord<1>{2, 0, 0, 0}, 1}), cubes[0]);
  EXPECT_EQ(Cube<1>({Coord<1>{1, 0, 0, 1}, 2}), cubes[1]);
  EXPECT_EQ(Cube<1>({Coord<1>{1, 0, 0, 0}, 3}), cubes[2]);
  EXPECT_EQ(Cube<1>({Coord<1>{2, 0, 1, 0}, 3}), cubes[3]);
  EXPECT_EQ(Cube<1>({Coord<1>{0, 0, 0, 0}, 4}), cubes[4]);
  EXPECT_EQ(Cube<1>({Coord<1>{1, 1, 0, 0}, 5}), cubes[5]);
  EXPECT_EQ(Cube<1>({Coord<1>{0, 0, 1, 0}, 5}), cubes[6]);
  EXPECT_EQ(Cube<1>({Coord<1>{2, 1, 1, 0}, 6}), cubes[7]);
  EXPECT_EQ(Cube<1>({Coord<1>{0, 0, 1, 1}, 6}), cubes[8]);
  EXPECT_EQ(Cube<1>({Coord<1>{2, 1, 0, 0}, 7}), cubes[9]);
  EXPECT_EQ(Cube<1>({Coord<1>{0, 0, 0, 1}, 7}), cubes[10]);
  EXPECT_EQ(Cube<1>({Coord<1>{1, 1, 0, 1}, 7}), cubes[11]);

  ASSERT_EQ(6, bitmap.cubes<2>().size());
}

TEST(Bitmap, cubes_2) {
  Bitmap bitmap(std::vector<int>{3, 3, 1});
  bitmap.load({
      0, 1, 2,
      6, 8, 7,
      3, 4, 5
    });

  std::vector<Cube<1>> cubes = bitmap.cubes<1>();
  ASSERT_EQ(12, cubes.size());
}

TEST(Bitmap, cofaces_1) {
  {
    Bitmap bitmap(std::vector<int>{1, 3, 3});
    bitmap.load({
        6, 7, 8,
        0, 1, 2,
        3, 4, 5
      });

    auto cofaces = bitmap.cofaces(Cube<1>(2, 0, 1, 0, 1));
    
    ASSERT_EQ(2, cofaces.size());
    EXPECT_EQ(Cube<2>(0, 0, 1, 0, 4), cofaces[0]);
    EXPECT_EQ(Cube<2>(0, 0, 0, 0, 7), cofaces[1]);
  }
}

TEST(Bitmap, cofaces_2) {
  {
    Bitmap bitmap(std::vector<int>{2, 2, 2});
    bitmap.load({1.0, 0.0,
                 3.0, 2.0,

                 4.0, 7.0,
                 5.0, 6.0,});

    auto cofaces = bitmap.cofaces(Cube<2>(0, 0, 0, 0, 3));
    ASSERT_EQ(1, cofaces.size());
    EXPECT_EQ(Cube<3>(0, 0, 0, 0, 7), cofaces[0]);
  }
}

TEST(Bitmap_Periodic, level) {
  using Bitmap = homccube::Bitmap<homccube::maxdim3::PolicyPeriodic3D>;
  using homccube::maxdim3::Periodicity;
  Bitmap bitmap(std::vector<int>{2, 2, 2}, Periodicity(0b111));

  bitmap.load({4.0, 7.0,
               5.0, 6.0,

               1.0, 0.0,
               3.0, 2.0,});
  
  ASSERT_EQ(4, bitmap.level(Coord<1>(0, 1, 0, 0)));
  ASSERT_EQ(6, bitmap.level(Coord<2>(1, 1, 1, 1)));
  ASSERT_EQ(7, bitmap.level(Coord<3>(0, 1, 1, 0)));
}

TEST(Bitmap_Periodic, cubes_000) {
  using Bitmap = homccube::Bitmap<homccube::maxdim3::PolicyPeriodic3D>;
  using homccube::maxdim3::Periodicity;
  Bitmap bitmap(std::vector<int>{2, 2, 2}, Periodicity(0b000));
  bitmap.load({1.0, 0.0,
               3.0, 2.0,

               4.0, 7.0,
               5.0, 6.0,});

  {
    std::vector<Cube<1>> cubes = bitmap.cubes<1>();
    ASSERT_EQ(12, cubes.size());
  }
  {
    std::vector<Cube<2>> cubes = bitmap.cubes<2>();
    ASSERT_EQ(6, cubes.size());
  }
  {
    std::vector<Cube<3>> cubes = bitmap.cubes<3>();
    ASSERT_EQ(1, cubes.size());
  }
}

TEST(Bitmap_Periodic, cubes_111) {
  using Bitmap = homccube::Bitmap<homccube::maxdim3::PolicyPeriodic3D>;
  using homccube::maxdim3::Periodicity;
  Bitmap bitmap(std::vector<int>{2, 2, 2}, Periodicity(0b111));
  bitmap.load({1.0, 0.0,
               3.0, 2.0,

               4.0, 7.0,
               5.0, 6.0,});

  {
    std::vector<Cube<1>> cubes = bitmap.cubes<1>();
    ASSERT_EQ(24, cubes.size());
  }
  {
    std::vector<Cube<2>> cubes = bitmap.cubes<2>();
    ASSERT_EQ(24, cubes.size());
  }
  {
    std::vector<Cube<3>> cubes = bitmap.cubes<3>();
    ASSERT_EQ(8, cubes.size());
  }
}

TEST(Bitmap_Periodic, cubes_101) {
  using Bitmap = homccube::Bitmap<homccube::maxdim3::PolicyPeriodic3D>;
  using homccube::maxdim3::Periodicity;
  Bitmap bitmap(std::vector<int>{2, 2, 2}, Periodicity(0b101));
  bitmap.load({1.0, 0.0,
               3.0, 2.0,

               4.0, 7.0,
               5.0, 6.0,});

  {
    std::vector<Cube<1>> cubes = bitmap.cubes<1>();
    ASSERT_EQ(20, cubes.size());
  }
  {
    std::vector<Cube<2>> cubes = bitmap.cubes<2>();
    ASSERT_EQ(16, cubes.size());
  }
  {
    std::vector<Cube<3>> cubes = bitmap.cubes<3>();
    ASSERT_EQ(4, cubes.size());
  }
}

TEST(Bitmap_Periodic, cofaces_111) {
  using Bitmap = homccube::Bitmap<homccube::maxdim3::PolicyPeriodic3D>;
  using homccube::maxdim3::Periodicity;
  Bitmap bitmap(std::vector<int>{2, 3, 4}, Periodicity(0b111));
  bitmap.load({0, 0, 0, 0,
               0, 0, 0, 0,
               0, 0, 0, 0,

               0, 0, 0, 0,
               0, 0, 0, 0,
               0, 0, 0, 0,});
  {
    auto cofaces = bitmap.cofaces(Cube<1>(0, 0, 0, 0, 3));
    ASSERT_EQ(4, cofaces.size());
    EXPECT_EQ(Coord<2>(1, 0, 0, 0), cofaces[0].c_);
    EXPECT_EQ(Coord<2>(1, 0, 0, 3), cofaces[1].c_);
    EXPECT_EQ(Coord<2>(2, 0, 0, 0), cofaces[2].c_);
    EXPECT_EQ(Coord<2>(2, 0, 2, 0), cofaces[3].c_);
  }
  {
    auto cofaces = bitmap.cofaces(Cube<1>(1, 0, 0, 0, 3));
    ASSERT_EQ(4, cofaces.size());
    EXPECT_EQ(Coord<2>(2, 0, 0, 0), cofaces[0].c_);
    EXPECT_EQ(Coord<2>(2, 1, 0, 0), cofaces[1].c_);
    EXPECT_EQ(Coord<2>(0, 0, 0, 0), cofaces[2].c_);
    EXPECT_EQ(Coord<2>(0, 0, 0, 3), cofaces[3].c_);
  }
  {
    auto cofaces = bitmap.cofaces(Cube<1>(2, 0, 0, 0, 3));
    ASSERT_EQ(4, cofaces.size());
    EXPECT_EQ(Coord<2>(0, 0, 0, 0), cofaces[0].c_);
    EXPECT_EQ(Coord<2>(0, 0, 2, 0), cofaces[1].c_);
    EXPECT_EQ(Coord<2>(1, 0, 0, 0), cofaces[2].c_);
    EXPECT_EQ(Coord<2>(1, 1, 0, 0), cofaces[3].c_);
  }

  {
    auto cofaces = bitmap.cofaces(Cube<2>(0, 0, 0, 0, 3));
    ASSERT_EQ(2, cofaces.size());
    EXPECT_EQ(Coord<3>(0, 0, 0, 0), cofaces[0].c_);
    EXPECT_EQ(Coord<3>(0, 1, 0, 0), cofaces[1].c_);
  }
  {
    auto cofaces = bitmap.cofaces(Cube<2>(1, 0, 0, 0, 3));
    ASSERT_EQ(2, cofaces.size());
    EXPECT_EQ(Coord<3>(0, 0, 0, 0), cofaces[0].c_);
    EXPECT_EQ(Coord<3>(0, 0, 2, 0), cofaces[1].c_);
  }
  {
    auto cofaces = bitmap.cofaces(Cube<2>(2, 0, 0, 0, 3));
    ASSERT_EQ(2, cofaces.size());
    EXPECT_EQ(Coord<3>(0, 0, 0, 0), cofaces[0].c_);
    EXPECT_EQ(Coord<3>(0, 0, 0, 3), cofaces[1].c_);
  }
}

TEST(Bitmap_Periodic, cofaces_011) {
  using Bitmap = homccube::Bitmap<homccube::maxdim3::PolicyPeriodic3D>;
  using homccube::maxdim3::Periodicity;
  Bitmap bitmap(std::vector<int>{2, 3, 4}, Periodicity(0b011));
  bitmap.load({0, 0, 0, 0,
               0, 0, 0, 0,
               0, 0, 0, 0,

               0, 0, 0, 0,
               0, 0, 0, 0,
               0, 0, 0, 0,});

  {
    auto cofaces = bitmap.cofaces(Cube<1>(0, 0, 0, 0, 3));
    ASSERT_EQ(3, cofaces.size());
    EXPECT_EQ(Coord<2>(1, 0, 0, 0), cofaces[0].c_);
    EXPECT_EQ(Coord<2>(2, 0, 0, 0), cofaces[1].c_);
    EXPECT_EQ(Coord<2>(2, 0, 2, 0), cofaces[2].c_);
  }
  {
    auto cofaces = bitmap.cofaces(Cube<1>(1, 0, 0, 0, 3));
    ASSERT_EQ(3, cofaces.size());
    EXPECT_EQ(Coord<2>(2, 0, 0, 0), cofaces[0].c_);
    EXPECT_EQ(Coord<2>(2, 1, 0, 0), cofaces[1].c_);
    EXPECT_EQ(Coord<2>(0, 0, 0, 0), cofaces[2].c_);
  }
  {
    auto cofaces = bitmap.cofaces(Cube<1>(2, 0, 0, 0, 3));
    ASSERT_EQ(4, cofaces.size());
    EXPECT_EQ(Coord<2>(0, 0, 0, 0), cofaces[0].c_);
    EXPECT_EQ(Coord<2>(0, 0, 2, 0), cofaces[1].c_);
    EXPECT_EQ(Coord<2>(1, 0, 0, 0), cofaces[2].c_);
    EXPECT_EQ(Coord<2>(1, 1, 0, 0), cofaces[3].c_);
  }

  {
    auto cofaces = bitmap.cofaces(Cube<2>(0, 0, 0, 0, 3));
    ASSERT_EQ(2, cofaces.size());
    EXPECT_EQ(Coord<3>(0, 0, 0, 0), cofaces[0].c_);
    EXPECT_EQ(Coord<3>(0, 1, 0, 0), cofaces[1].c_);
  }
  {
    auto cofaces = bitmap.cofaces(Cube<2>(1, 0, 0, 0, 3));
    ASSERT_EQ(2, cofaces.size());
    EXPECT_EQ(Coord<3>(0, 0, 0, 0), cofaces[0].c_);
    EXPECT_EQ(Coord<3>(0, 0, 2, 0), cofaces[1].c_);
  }
  {
    auto cofaces = bitmap.cofaces(Cube<2>(2, 0, 0, 0, 3));
    ASSERT_EQ(1, cofaces.size());
    EXPECT_EQ(Coord<3>(0, 0, 0, 0), cofaces[0].c_);
  }
}
