#include <gtest/gtest.h>
#include "basic_types.hpp"

#include <sstream>

using namespace homccube::maxdim3;

TEST(Coord, Coord) {
  ASSERT_EQ(4, sizeof(Coord<0>));
  ASSERT_EQ(4, sizeof(Coord<1>));
  ASSERT_EQ(4, sizeof(Coord<2>));
  ASSERT_EQ(4, sizeof(Coord<3>));
}

TEST(Coord, coord_id) {
  Coord<1> coord = {1, 129, 1000, 198};
  EXPECT_EQ(1, coord.m_);
  EXPECT_EQ(129, coord.x_);
  EXPECT_EQ(1000, coord.y_);
  EXPECT_EQ(198, coord.z_);
  EXPECT_EQ((1u) | (129u << 2) | (1000u << 12) | (198u << 22), coord.coord_id_);
}

TEST(Coord, io) {
  std::stringstream ss;
  ss << Coord<1>{1, 129, 1000, 198};
  EXPECT_EQ("Coord<1>[1: 129, 1000, 198]", ss.str());
}

TEST(Cube, Cube) {
  ASSERT_EQ(8, sizeof(Cube<0>));
  ASSERT_EQ(8, sizeof(Cube<1>));
  ASSERT_EQ(8, sizeof(Cube<2>));
  ASSERT_EQ(8, sizeof(Cube<3>));
}

TEST(Coord, order) {
  Cube<1> cube = {Coord<1>{1, 1000, 999, 998}, 800000};
  EXPECT_EQ((800000ul<<32) | cube.c_.coord_id_, cube.order_);
}

TEST(Shape, vertices) {
  Shape shape(std::vector<int>{3, 3, 3});
  Index x, y;
  shape.vertices(Cube<1>(0, 1, 1, 1, 0), x, y);
  EXPECT_EQ(shape.index(Coord<0>{0, 1, 1, 1}), x);
  EXPECT_EQ(shape.index(Coord<0>{0, 2, 1, 1}), y);

  shape.vertices(Cube<1>(1, 1, 1, 1, 0), x, y);
  EXPECT_EQ(shape.index(Coord<0>{0, 1, 1, 1}), x);
  EXPECT_EQ(shape.index(Coord<0>{0, 1, 2, 1}), y);

  shape.vertices(Cube<1>(2, 1, 1, 1, 0), x, y);
  EXPECT_EQ(shape.index(Coord<0>{0, 1, 1, 1}), x);
  EXPECT_EQ(shape.index(Coord<0>{0, 1, 1, 2}), y);
}

TEST(Shape, foreach_cofaces_1) {
  Shape shape(std::vector<int>{3, 4, 5});
  {
    std::vector<Coord<2>> cofaces;
    shape.foreach_coface(Coord<1>{0, 1, 1, 1},
                         [&](Coord<2> c){ cofaces.push_back(c);});
    ASSERT_EQ(4, cofaces.size());
    EXPECT_EQ(Coord<2>(1, 1, 1, 1), cofaces[0]);
    EXPECT_EQ(Coord<2>(1, 1, 1, 0), cofaces[1]);
    EXPECT_EQ(Coord<2>(2, 1, 1, 1), cofaces[2]);
    EXPECT_EQ(Coord<2>(2, 1, 0, 1), cofaces[3]);
  }
  {
    std::vector<Coord<2>> cofaces;
    shape.foreach_coface(Coord<1>{1, 1, 1, 1},
                         [&](Coord<2> c){ cofaces.push_back(c);});
    ASSERT_EQ(4, cofaces.size());
    EXPECT_EQ(Coord<2>(2, 1, 1, 1), cofaces[0]);
    EXPECT_EQ(Coord<2>(2, 0, 1, 1), cofaces[1]);
    EXPECT_EQ(Coord<2>(0, 1, 1, 1), cofaces[2]);
    EXPECT_EQ(Coord<2>(0, 1, 1, 0), cofaces[3]);
  }
  {
    std::vector<Coord<2>> cofaces;
    shape.foreach_coface(Coord<1>{2, 1, 1, 1},
                         [&](Coord<2> c){ cofaces.push_back(c);});
    ASSERT_EQ(4, cofaces.size());
    EXPECT_EQ(Coord<2>(0, 1, 1, 1), cofaces[0]);
    EXPECT_EQ(Coord<2>(0, 1, 0, 1), cofaces[1]);
    EXPECT_EQ(Coord<2>(1, 1, 1, 1), cofaces[2]);
    EXPECT_EQ(Coord<2>(1, 0, 1, 1), cofaces[3]);
  }
  {
    std::vector<Coord<2>> cofaces;
    shape.foreach_coface(Coord<1>{2, 0, 0, 0},
                         [&](Coord<2> c){ cofaces.push_back(c);});
    ASSERT_EQ(2, cofaces.size());
    EXPECT_EQ(Coord<2>(0, 0, 0, 0), cofaces[0]);
    EXPECT_EQ(Coord<2>(1, 0, 0, 0), cofaces[1]);
  }
  {
    std::vector<Coord<2>> cofaces;
    shape.foreach_coface(Coord<1>{2, 2, 3, 3},
                         [&](Coord<2> c){ cofaces.push_back(c);});
    ASSERT_EQ(2, cofaces.size());
    EXPECT_EQ(Coord<2>(0, 2, 2, 3), cofaces[0]);
    EXPECT_EQ(Coord<2>(1, 1, 3, 3), cofaces[1]);
  }

}

TEST(Shape, foreach_cofaces_2) {
  Shape shape(std::vector<int>{3, 4, 5});
  {
    std::vector<Coord<3>> cofaces;
    shape.foreach_coface(Coord<2>{0, 1, 1, 1},
                         [&](Coord<3> c){ cofaces.push_back(c);});
    ASSERT_EQ(2, cofaces.size());
    EXPECT_EQ(Coord<3>(0, 1, 1, 1), cofaces[0]);
    EXPECT_EQ(Coord<3>(0, 0, 1, 1), cofaces[1]);
  }
  {
    std::vector<Coord<3>> cofaces;
    shape.foreach_coface(Coord<2>{1, 1, 1, 1},
                         [&](Coord<3> c){ cofaces.push_back(c);});
    ASSERT_EQ(2, cofaces.size());
    EXPECT_EQ(Coord<3>(0, 1, 1, 1), cofaces[0]);
    EXPECT_EQ(Coord<3>(0, 1, 0, 1), cofaces[1]);
  }
  {
    std::vector<Coord<3>> cofaces;
    shape.foreach_coface(Coord<2>{2, 1, 1, 1},
                         [&](Coord<3> c){ cofaces.push_back(c);});
    ASSERT_EQ(2, cofaces.size());
    EXPECT_EQ(Coord<3>(0, 1, 1, 1), cofaces[0]);
    EXPECT_EQ(Coord<3>(0, 1, 1, 0), cofaces[1]);
  }
  {
    std::vector<Coord<3>> cofaces;
    shape.foreach_coface(Coord<2>{1, 0, 0, 0},
                         [&](Coord<3> c){ cofaces.push_back(c);});
    ASSERT_EQ(1, cofaces.size());
    EXPECT_EQ(Coord<3>(0, 0, 0, 0), cofaces[0]);
  }
  {
    std::vector<Coord<3>> cofaces;
    shape.foreach_coface(Coord<2>{1, 1, 3, 3},
                         [&](Coord<3> c){ cofaces.push_back(c);});
    ASSERT_EQ(1, cofaces.size());
    EXPECT_EQ(Coord<3>(0, 1, 2, 3), cofaces[0]);
  }

}

TEST(PeriodicShape, vertices) {
  PeriodicShape shape(std::vector<int>{3, 4, 5}, Periodicity(0b111));
  Index x, y;
  shape.vertices(Cube<1>(0, 1, 1, 1, 0), x, y);
  EXPECT_EQ(shape.index(Coord<0>{0, 1, 1, 1}), x);
  EXPECT_EQ(shape.index(Coord<0>{0, 2, 1, 1}), y);

  shape.vertices(Cube<1>(0, 2, 3, 4, 0), x, y);
  EXPECT_EQ(shape.index(Coord<0>{0, 2, 3, 4}), x);
  EXPECT_EQ(shape.index(Coord<0>{0, 0, 3, 4}), y);

  shape.vertices(Cube<1>(1, 2, 3, 4, 0), x, y);
  EXPECT_EQ(shape.index(Coord<0>{0, 2, 3, 4}), x);
  EXPECT_EQ(shape.index(Coord<0>{0, 2, 0, 4}), y);

  shape.vertices(Cube<1>(2, 2, 3, 4, 0), x, y);
  EXPECT_EQ(shape.index(Coord<0>{0, 2, 3, 4}), x);
  EXPECT_EQ(shape.index(Coord<0>{0, 2, 3, 0}), y);
}
