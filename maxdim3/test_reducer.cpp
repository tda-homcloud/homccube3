#include <gtest/gtest.h>
#include "types.hpp"
#include "../config.hpp"

using namespace homccube::maxdim3::types;
using Config = homccube::Config;

class ReducerTest: public ::testing::TestWithParam<Config> {
};

TEST_P(ReducerTest, test_1) {
  Config config = GetParam();

  Bitmap bitmap(std::vector<int>{1, 3, 3});
  PD pd;
  bitmap.load({
      0, 1, 2,
      6, 8, 7,
      3, 4, 5
    });
  LinkFind link_find(bitmap, &pd);
  link_find.compute();

  Reducer<1> reducer(bitmap, *link_find.survivors_, config, &pd);
  reducer.compute();

  ASSERT_EQ(pd.size(), 3);
  EXPECT_EQ(1, pd.degrees_[2]);
  EXPECT_EQ(7, pd.births_[2]);
  EXPECT_EQ(8, pd.deaths_[2]);

  ASSERT_EQ(0, reducer.upper_survivors()->size());
}

TEST_P(ReducerTest, test_2) {
  Config config = GetParam();

  Bitmap bitmap(std::vector<int>{2, 3, 3});
  bitmap.load({
      0, 3, 8,
      4, 16, 5,
      13, 11, 6,

      1, 9, 7,
      15, 17, 10,
      2, 12, 14,
    });
  PD pd0;
  LinkFind link_find(bitmap, &pd0);
  link_find.compute();

  ASSERT_EQ(12 * 2 + 9 - 17, link_find.survivors_->size());

  PD pd1;
  Reducer<1> reducer(bitmap, *link_find.survivors_, config, &pd1);
  reducer.compute();

  ASSERT_EQ(1, pd1.size());
  ASSERT_EQ(13, pd1.births_[0]);
  ASSERT_EQ(16, pd1.deaths_[0]);

  ASSERT_EQ(4, reducer.upper_survivors()->size());
}

TEST_P(ReducerTest, test_3) {
  Config config = GetParam();

  Bitmap bitmap(std::vector<int>{3, 3, 3});
  bitmap.load({
      0 , 12, 1 ,
      10, 25, 19,
      2 , 15, 3 ,

      8 , 18, 13,
      22, 26, 23,
      9 , 21, 17,
      
      4 , 14, 5 ,
      11, 24, 20,
      6 , 16, 7 ,
    });

  PD pd0;
  LinkFind link_find(bitmap, &pd0);
  link_find.compute();

  PD pd1;
  Reducer<1> reducer1(bitmap, *link_find.survivors_, config, &pd1);
  reducer1.compute();
  auto survivors1 = reducer1.upper_survivors();
  ASSERT_EQ(5, pd1.size());
  EXPECT_EQ(20, pd1.births_[0]);
  EXPECT_EQ(23, pd1.deaths_[0]);
  EXPECT_EQ(19, pd1.births_[1]);
  EXPECT_EQ(24, pd1.deaths_[1]);
  EXPECT_EQ(17, pd1.births_[2]);
  EXPECT_EQ(21, pd1.deaths_[2]);
  EXPECT_EQ(14, pd1.births_[3]);
  EXPECT_EQ(18, pd1.deaths_[3]);
  EXPECT_EQ(11, pd1.births_[4]);
  EXPECT_EQ(22, pd1.deaths_[4]);
  ASSERT_EQ(8, survivors1->size());

  PD pd2;
  Reducer<2> reducer2(bitmap, *survivors1, config, &pd2);
  reducer2.compute();
  
  ASSERT_EQ(1, pd2.births_.size());
  EXPECT_EQ(25, pd2.births_[0]);
  EXPECT_EQ(26, pd2.deaths_[0]);
}

INSTANTIATE_TEST_CASE_P(ParameterizedTest, ReducerTest,
                        ::testing::Values(Config{0, false},
                                          Config{1, false},
                                          Config{2, false},
                                          Config{3, false},
                                          Config{100000, false},
                                          Config{0, true},
                                          Config{1, true},
                                          Config{2, true},
                                          Config{3, true},
                                          Config{100000, true}
                                          ));
