#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <vector>

#include "../config.hpp"
#include "../dipha_format.hpp"
#include "../bitmap.hpp"
#include "../pd.hpp"
#include "../cubemap.hpp"
#include "../link_find.hpp"
#include "../reducer.hpp"

#include "basic_types.hpp"

namespace dipha = homccube::dipha;

using Policy3D = homccube::maxdim3::Policy3D;
template <int d> using Cube = homccube::maxdim3::Cube<d>;
using Bitmap = homccube::Bitmap<Policy3D>;
using PD = homccube::PD<Policy3D>;
using LinkFind = homccube::LinkFind<Policy3D>;
template<int dim> using Reducer = homccube::Reducer<dim, Policy3D>;
using homccube::Config;

void show_help() {
  std::cout << "Usage: cr3 TH INPUT OUTPUT" << std::endl;
  std::exit(1);
}

Config build_config(const std::string& threshold) {
  return Config {std::stoi(threshold), false};
}

std::shared_ptr<std::vector<Cube<1>>>
compute_0th_pd(const Bitmap& bitmap, PD* pd) {
  LinkFind link_find(bitmap, pd);
  link_find.compute();
  return link_find.survivors_;
}

template<int d>
std::shared_ptr<std::vector<Cube<d + 1>>>
compute_kth_pd(const Bitmap& bitmap,
               const std::vector<Cube<d>>& survivors,
               const Config& config, PD* pd) {
  Reducer<d> reducer(bitmap, survivors, config, pd);
  reducer.compute();
  return reducer.upper_survivors();
}
               
void run(const std::vector<int>& shape, const Config& config,
         std::istream* in, std::ostream* out) {
  Bitmap bitmap(shape);
  bitmap.load(in);

  PD pd;
  auto survivors0 = compute_0th_pd(bitmap, &pd);
  std::cout << "PD0 done" << std::endl;
  auto survivors1 = compute_kth_pd<1>(bitmap, *survivors0, config, &pd);
  std::cout << "PD1 done" << std::endl;
  survivors0 = nullptr;
  compute_kth_pd<2>(bitmap, *survivors1, config, &pd);
  std::cout << "PD2 done" << std::endl;

  pd.write_dipha_format(bitmap, out);
}

int main(int argc, char** argv) {
  if (argc != 4)
    show_help();

  Config config = build_config(argv[1]);

  std::ifstream fin(argv[2], std::ios::binary);
  auto shape = dipha::read_metadata(&fin);

  std::ofstream fout(argv[3], std::ios::binary);
  run(shape, config, &fin, &fout);
}
