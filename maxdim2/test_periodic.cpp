#include <gtest/gtest.h>

#include "basic_types.hpp"
#include "../bitmap.hpp"
#include "../config.hpp"
#include "../bitmap.hpp"
#include "../pd.hpp"
#include "../link_find.hpp"
#include "../reducer.hpp"

using namespace homccube::maxdim2;

using Bitmap = homccube::Bitmap<PolicyPeriodic2D>;
using PD = homccube::PD<PolicyPeriodic2D>;
using LinkFind = homccube::LinkFind<PolicyPeriodic2D>;
template<int dim> using Reducer = homccube::Reducer<dim, PolicyPeriodic2D>;
using homccube::Config;

TEST(PeriodicShape, vertices) {
  PeriodicShape shape(std::vector<int>{2, 3}, Periodicity(0b11));
  Index v0, v1;
  shape.vertices(Cube<1>(0, 1, 2, 0), v0, v1);
  EXPECT_EQ(shape.index0(Coord<0>(0, 1, 2)), v0);
  EXPECT_EQ(shape.index0(Coord<0>(0, 0, 2)), v1);

  shape.vertices(Cube<1>(1, 1, 2, 0), v0, v1);
  EXPECT_EQ(shape.index0(Coord<0>(0, 1, 2)), v0);
  EXPECT_EQ(shape.index0(Coord<0>(0, 1, 0)), v1);
}

TEST(PeriodicShape, get_level) {
  Bitmap bitmap(std::vector<int>{2, 3}, Periodicity(0b11));
  bitmap.load({5, 0, 1,
               2, 3, 4});
  EXPECT_EQ(4, bitmap.level(Coord<0>(0, 1, 2)));

  EXPECT_EQ(5, bitmap.level(Coord<1>(0, 1, 0)));
  EXPECT_EQ(4, bitmap.level(Coord<1>(1, 1, 2)));

  EXPECT_EQ(5, bitmap.level(Coord<2>(0, 1, 0)));
  EXPECT_EQ(5, bitmap.level(Coord<2>(0, 1, 2)));
}

TEST(PeriodicShape, foreach_coord) {
  {
    std::vector<Coord<1>> coords;
    PeriodicShape shape(std::vector<int>{2, 3}, Periodicity(0b10));
    shape.foreach_coords<1>([&coords](const Coord<1> c){ coords.push_back(c); });
    std::cout << shape.periodicity_[0] << " " << shape.periodicity_[1] << std::endl;
    ASSERT_EQ(9, coords.size());
  }
  {
    std::vector<Coord<1>> coords;
    PeriodicShape shape(std::vector<int>{2, 3}, Periodicity(0b11));
    shape.foreach_coords<1>([&coords](const Coord<1> c){ coords.push_back(c); });
    ASSERT_EQ(12, coords.size());
  }

  {
    std::vector<Coord<2>> coords;
    PeriodicShape shape(std::vector<int>{2, 3}, Periodicity(0b10));
    shape.foreach_coords<2>([&coords](const Coord<2> c){ coords.push_back(c); });
    ASSERT_EQ(3, coords.size());
    EXPECT_EQ(Coord<2>(0, 0, 0), coords[0]);
    EXPECT_EQ(Coord<2>(0, 0, 1), coords[1]);
    EXPECT_EQ(Coord<2>(0, 0, 2), coords[2]);
  }
  {
    std::vector<Coord<2>> coords;
    PeriodicShape shape(std::vector<int>{2, 3}, Periodicity(0b11));
    shape.foreach_coords<2>([&coords](const Coord<2> c){ coords.push_back(c); });
    ASSERT_EQ(6, coords.size());
    EXPECT_EQ(Coord<2>(0, 0, 0), coords[0]);
    EXPECT_EQ(Coord<2>(0, 0, 1), coords[1]);
    EXPECT_EQ(Coord<2>(0, 0, 2), coords[2]);
    EXPECT_EQ(Coord<2>(0, 1, 0), coords[3]);
    EXPECT_EQ(Coord<2>(0, 1, 1), coords[4]);
    EXPECT_EQ(Coord<2>(0, 1, 2), coords[5]);
  }
}

std::vector<Coord<2>> cofaces(const PeriodicShape& shape, const Coord<1> c) {
  std::vector<Coord<2>> coords;

  shape.foreach_coface(c, [&coords](const Coord<2> c) {
                         coords.push_back(c);
                       });
  return coords;
}

TEST(PeriodicShape, foreach_coface) {
  {
    PeriodicShape shape(std::vector<int>{2, 3}, Periodicity(0b10));
    std::vector<Coord<2>> coords = cofaces(shape, Coord<1>(0, 0, 1));
    ASSERT_EQ(2, coords.size());
    EXPECT_EQ(Coord<2>(0, 0, 1), coords[0]);
    EXPECT_EQ(Coord<2>(0, 0, 0), coords[1]);
  }
  {
    PeriodicShape shape(std::vector<int>{2, 3}, Periodicity(0b10));
    std::vector<Coord<2>> coords = cofaces(shape, Coord<1>(1, 0, 0));
    ASSERT_EQ(1, coords.size());
    EXPECT_EQ(Coord<2>(0, 0, 0), coords[0]);
  }
  {
    PeriodicShape shape(std::vector<int>{2, 3}, Periodicity(0b10));
    std::vector<Coord<2>> coords = cofaces(shape, Coord<1>(0, 0, 0));
    ASSERT_EQ(2, coords.size());
    EXPECT_EQ(Coord<2>(0, 0, 0), coords[0]);
    EXPECT_EQ(Coord<2>(0, 0, 2), coords[1]);
  }
}

TEST(Reducer_Periodic, test) {
  Config config = {1, false};

  Bitmap bitmap(std::vector<int>{2, 3}, Periodicity(0b11));
  bitmap.load({1, 5, 3,
               0, 4, 2});
  PD pd0;
  
  LinkFind link_find(bitmap, &pd0);
  link_find.compute();

  ASSERT_EQ(1, pd0.size());
  EXPECT_EQ(0, pd0.births_[0]);
  EXPECT_EQ(INFINITE_LEVEL, pd0.deaths_[0]);

  PD pd1;
  Reducer<1> reducer1(bitmap, *link_find.survivors_, config, &pd1);
  reducer1.compute();

  ASSERT_EQ(2, pd1.size());
  EXPECT_EQ(4, pd1.births_[0]);
  EXPECT_EQ(INFINITE_LEVEL, pd1.deaths_[0]);
  EXPECT_EQ(1, pd1.births_[1]);
  EXPECT_EQ(INFINITE_LEVEL, pd1.deaths_[1]);

  auto survivors1 = reducer1.upper_survivors();
  ASSERT_EQ(1, survivors1->size());
  ASSERT_EQ(5, survivors1->at(0).level_);
}
