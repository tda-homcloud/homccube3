#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <vector>

#include "../config.hpp"
#include "../dipha_format.hpp"
#include "../bitmap.hpp"
#include "../pd.hpp"
#include "../cubemap.hpp"
#include "../link_find.hpp"
#include "../reducer.hpp"

#include "basic_types.hpp"

namespace dipha = homccube::dipha;

using Policy2D = homccube::maxdim2::Policy2D;
template <int d> using Cube = homccube::maxdim2::Cube<d>;
using Bitmap = homccube::Bitmap<Policy2D>;
using PD = homccube::PD<Policy2D>;
using LinkFind = homccube::LinkFind<Policy2D>;
template<int dim> using Reducer = homccube::Reducer<dim, Policy2D>;
using homccube::Config;

void show_help() {
  std::cout << "Usage: cr2 TH INPUT OUTPUT" << std::endl;
  std::exit(1);
}

Config build_config(const std::string& threshold) {
  return Config {std::stoi(threshold)};
}

std::shared_ptr<std::vector<Cube<1>>>
compute_0th_pd(const Bitmap& bitmap, PD* pd) {
  LinkFind link_find(bitmap, pd);
  link_find.compute();
  return link_find.survivors_;
}

std::shared_ptr<std::vector<Cube<2>>>
compute_1st_pd(const Bitmap& bitmap,
               const std::vector<Cube<1>>& survivors,
               const Config& config, PD* pd) {
  Reducer<1> reducer(bitmap, survivors, config, pd);
  reducer.compute();
  return reducer.upper_survivors();
}
               
void run(const std::vector<int>& shape, const Config& config,
         std::istream* in, std::ostream* out) {
  Bitmap bitmap(shape);
  bitmap.load(in);

  PD pd;
  auto survivors0 = compute_0th_pd(bitmap, &pd);
  std::cout << "PD0 done" << std::endl;
  auto survivors1 = compute_1st_pd(bitmap, *survivors0, config, &pd);
  std::cout << "PD1 done" << std::endl;

  pd.write_dipha_format(bitmap, out);
}

int main(int argc, char** argv) {
  if (argc != 4)
    show_help();

  Config config = build_config(argv[1]);

  std::ifstream fin(argv[2], std::ios::binary);
  auto shape = dipha::read_metadata(&fin);

  std::ofstream fout(argv[3], std::ios::binary);
  run(shape, config, &fin, &fout);
}
