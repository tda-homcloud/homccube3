#include <gtest/gtest.h>
#include "basic_types.hpp"
#include "../bitmap.hpp"

using namespace homccube::maxdim2;

using Bitmap = homccube::Bitmap<Policy2D>;

TEST(Coord, Coord) {
  ASSERT_EQ(4, sizeof(Coord<0>));
  ASSERT_EQ(4, sizeof(Coord<1>));
  ASSERT_EQ(4, sizeof(Coord<2>));
}

TEST(Cube, Cube) {
  ASSERT_EQ(8, sizeof(Cube<0>));
  ASSERT_EQ(8, sizeof(Cube<1>));
  ASSERT_EQ(8, sizeof(Cube<2>));
}

TEST(Shape, vertices) {
  Shape shape(std::vector<int>{3, 3});
  Index x, y;

  shape.vertices(Cube<1>(0, 1, 1, 0), x, y);
  EXPECT_EQ(shape.index(Coord<0>(0, 1, 1)), x);
  EXPECT_EQ(shape.index(Coord<0>(0, 2, 1)), y);

  shape.vertices(Cube<1>(1, 1, 1, 0), x, y);
  EXPECT_EQ(shape.index(Coord<0>(0, 1, 1)), x);
  EXPECT_EQ(shape.index(Coord<0>(0, 1, 2)), y);
}


TEST(Shape, get_level) {
  Bitmap bitmap(std::vector<int>{3, 3});
  bitmap.load({0, 1, 2,
               6, 8, 7,
               3, 4, 5});
  ASSERT_EQ(6, bitmap.level(Coord<0>{0, 1, 0}));
  
  ASSERT_EQ(6, bitmap.level(Coord<1>{0, 0, 0}));
  ASSERT_EQ(1, bitmap.level(Coord<1>{1, 0, 0}));

  
  ASSERT_EQ(8, bitmap.level(Coord<2>{0, 0, 0}));
}

TEST(Shape, foreach_coords) {
  Shape shape(std::vector<int>{2, 4});
  std::vector<Coord<1>> coords;
  shape.foreach_coords<1>([&coords](Coord<1> c){
                            coords.push_back(c);
                          });
  ASSERT_EQ(10, coords.size());
  EXPECT_EQ(Coord<1>(0, 0, 0), coords[0]);
  EXPECT_EQ(Coord<1>(1, 0, 0), coords[1]);
  EXPECT_EQ(Coord<1>(1, 1, 0), coords[7]);
  EXPECT_EQ(Coord<1>(1, 1, 1), coords[8]);
  EXPECT_EQ(Coord<1>(1, 1, 2), coords[9]);
}
