#!/bin/zsh

HOMCCUBE3D=../build/maxdim3/cr3
HOMCCUBE2D=../build/maxdim2/cr2
DIPHA=dipha
CR=../../cubicalripser/cr3_kaji/cubicalripser

cd $(dirname $0)

# echo "- Random 50x50x50"
# time $HOMCCUBE3D 2 c50.complex c50-homccube.diagram
# # time $DIPHA c50.complex c50-dipha.diagram

# for i in 0 1 2 3; do
#     echo "degree $i"
#     diff <(homcloud-dump-diagram -d $i c50-homccube.diagram|sort) \
#          <(homcloud-dump-diagram -d $i c50-dipha.diagram|sort)
# done

# echo "- Lena x 9"
# time $HOMCCUBE3D 2 lena9.complex lena9-homccube.diagram
# time $CR --output lena9-cr.diagram lena9.complex
# time $DIPHA lena9.complex lena9-dipha.diagram

# echo "homccube vs cubicalripser"
# for i in 0 1 2 3 4; do
#     echo "degree $i"
#     diff <(homcloud-dump-diagram -d $i lena9-homccube.diagram|sort) \
#          <(homcloud-dump-diagram -d $i lena9-cr.diagram|sort)
# done

# echo "homccube vs dipha"
# for i in 0 1 2 3 4; do
#     echo "degree $i"
#     diff <(homcloud-dump-diagram -d $i lena9-homccube.diagram|sort) \
#          <(homcloud-dump-diagram -d $i lena9-dipha.diagram|sort)
# done

# echo "- distance_transform (indexed)"
# time $HOMCCUBE3D requested distance_transform.complex distance_transform-homccube.diagram
# time $CR --output distance_transform-cr.diagram distance_transform.complex
# time $DIPHA distance_transform.complex distance_transform-dipha.diagram

# echo "homccube vs cubicalripser"
# for i in 0 1 2 3 4; do
#     echo "degree $i"
#     diff <(homcloud-dump-diagram -d $i distance_transform-homccube.diagram|sort) \
#          <(homcloud-dump-diagram -d $i distance_transform-cr.diagram|sort)
# done

# echo "homccube vs dipha"
# for i in 0 1 2 3 4; do
#     echo "degree $i"
#     diff <(homcloud-dump-diagram -d $i distance_transform-homccube.diagram|sort) \
#          <(homcloud-dump-diagram -d $i distance_transform-dipha.diagram|sort)
# done

echo "- 100x100"
time $DIPHA c100x100.complex c100x100-dipha.diagram
time $HOMCCUBE2D 1 c100x100.complex c100x100-homccube.diagram

for i in 0 1 2 3 4; do
    echo "degree $i"
    diff <(homcloud-dump-diagram -d $i c100x100-homccube.diagram|sort) \
         <(homcloud-dump-diagram -d $i c100x100-dipha.diagram|sort)
done

# echo "- 1000x1000"
# time $DIPHA c1000x1000.complex c1000x1000-dipha.diagram
# time $HOMCCUBE3D requested c1000x1000.complex c1000x1000-homccube.diagram

# for i in 0 1 2 3 4; do
#     echo "degree $i"
#     diff <(homcloud-dump-diagram -d $i c1000x1000-homccube.diagram|sort) \
#          <(homcloud-dump-diagram -d $i c1000x1000-dipha.diagram|sort)
# done

# echo "- 40x40x40x40"
# time $DIPHA c40x40x40x40.complex c40x40x40x40-dipha.diagram
# time $HOMCCUBE3D always c40x40x40x40.complex c40x40x40x40-homccube.diagram

# for i in 0 1 2 3 4; do
#     echo "degree $i"
#     diff <(homcloud-dump-diagram -d $i c40x40x40x40-homccube.diagram|sort) \
#          <(homcloud-dump-diagram -d $i c40x40x40x40-dipha.diagram|sort)
# done

# echo "- 20x20x20x20"
# time $DIPHA c20x20x20x20.complex c20x20x20x20-dipha.diagram
# time $HOMCCUBE3D always c20x20x20x20.complex c20x20x20x20-homccube.diagram

# for i in 0 1 2 3 4; do
#     echo "degree $i"
#     diff <(homcloud-dump-diagram -d $i c20x20x20x20-homccube.diagram|sort) \
#          <(homcloud-dump-diagram -d $i c20x20x20x20-dipha.diagram|sort)
# done

# echo "- bin 20^4"
# time $DIPHA bin20~4.complex bin20~4-dipha.diagram
# time $HOMCCUBE3D requested bin20~4.complex bin20~4-homccube.diagram

# for i in 0 1 2 3 4; do
#     echo "degree $i"
#     diff <(homcloud-dump-diagram -d $i bin20~4-homccube.diagram|sort) \
#          <(homcloud-dump-diagram -d $i bin20~4-dipha.diagram|sort)
# done

# echo "- bin 40^4"
# # time $DIPHA bin40~4.complex bin40~4-dipha.diagram
# time $HOMCCUBE3D always bin40~4.complex bin40~4-homccube.diagram

# for i in 0 1 2 3 4; do
#     echo "degree $i"
#     diff <(homcloud-dump-diagram -d $i bin40~4-homccube.diagram|sort) \
#          <(homcloud-dump-diagram -d $i bin40~4-dipha.diagram|sort)
# done
