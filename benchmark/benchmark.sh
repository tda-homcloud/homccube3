#!/bin/dash

HOMCCUBE=../build/maxdim3/cr3
DIPHA=dipha
CR=../../cubicalripser/cr3_kaji/cubicalripser

cd `dirname $0`

# /usr/bin/time -v $HOMCCUBE 0 lena9.complex lena9-homccube.diagram
# /usr/bin/time -v $HOMCCUBE 1 lena9.complex lena9-homccube.diagram
# /usr/bin/time -v $HOMCCUBE 2 lena9.complex lena9-homccube.diagram
# /usr/bin/time -v $CR --output lena9-cr.diagram lena9.complex
# /usr/bin/time -v ${CR}-clang --output lena9-cr.diagram lena9.complex
# /usr/bin/time -v ${CR}-g++ --output lena9-cr.diagram lena9.complex
# /usr/bin/time -v $DIPHA lena9.complex lena9-dipha.diagram


# /usr/bin/time -v $HOMCCUBE 0 fales cf.complex cf-homccube.diagram
/usr/bin/time -v $HOMCCUBE 1 cf.complex cf-homccube.diagram
# /usr/bin/time -v $HOMCCUBE 2 cf.complex cf-homccube.diagram
/usr/bin/time -v ${CR} --output cf-cr.diagram cf.complex
# /usr/bin/time -v $DIPHA cf.complex cf-dipha.diagram


# /usr/bin/time -v $HOMCCUBE 0 c50.complex c50-homccube.diagram
# /usr/bin/time -v $HOMCCUBE 1 c50.complex c50-homccube.diagram
# /usr/bin/time -v $HOMCCUBE 2 c50.complex c50-homccube.diagram
# /usr/bin/time -v $DIPHA c50.complex c50-dipha.diagram


# /usr/bin/time -v $HOMCCUBE 0 c100.complex c100-homccube.diagram
# /usr/bin/time -v $HOMCCUBE 1 c100.complex c100-homccube.diagram
# /usr/bin/time -v $HOMCCUBE 2 c100.complex c100-homccube.diagram
# /usr/bin/time -v $CR --output c100-cr.diagram c100.complex
# /usr/bin/time -v $DIPHA c100.complex c100-dipha.diagram
