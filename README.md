# HomCCube3

This is software for computing persistence diagrams for 2D and 3D cubical filtration. Anthoer version of https://bitbucket.org/tda-homcloud/homccube/ .

## How to build

1. Install cmake and boost library
2. Type `cmake .` to generate Makefile
3. Type `make` to build
4. Type `make test` for unit tests
5. A generated file `maxdim2/cr2` and `maxdim3/cr3` is an executable file

`cr2` is for 2D bitmaps, and `cr3` is for 3D bitmaps.

## How to run `cr2` and `cr3`

The commandline operation is as follows:

    ./cr3 1 input.complex output.diagram
    
where `input.complex` is the dipha's bitmap data file and `output.diagram` is a
persistence diagram whose format is dipha's format.

The number `1` is an algorithm parameter. You can choose 0, 1, 2, ....
On the average, `1` gives the best performance.


## Acknowledgement

Some mathematical and technical ideas are based on
CubicalRipser <https://github.com/CubicalRipser> by Takeshi Sudo and
Kazushi Ahara. 

I also thank Kaji-san for the discussion about the performance of CubicalRipser.

## License

This software is distributed under GPL version 3, or any later version.

## Author

Ippei Obayashi <ippei.obayashi@riken.jp>
